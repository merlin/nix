#!/usr/bin/env sh
HOSTNAME=$1
if [ -z "$HOSTNAME" ]; then
    echo "Build a NixOS host remotely, and show the package changes compared to the production version."
    echo "Usage: $0 <hostname> [--dry-run]"
    exit 1
fi

BUILD_HOST="$HOSTNAME.tardisproject.uk"
STORE="ssh-ng://$BUILD_HOST"
REMOTE_BUILDER="ssh://$BUILD_HOST x86_64-linux - 10 1 kvm,benchmark"

set -ex

# Build host remotely
OUT_PATH=`nix build --no-link --no-use-registries --accept-flake-config --print-out-paths \
    --eval-store auto --store "$STORE" \
    --builders "$REMOTE_BUILDER" --max-jobs 0 \
    ".#nixosConfigurations.${HOSTNAME}.config.system.build.toplevel"`

ssh $BUILD_HOST nvd --color always diff /run/current-system $OUT_PATH
