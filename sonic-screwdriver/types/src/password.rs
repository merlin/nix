use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct Password(String);

impl Password {
    pub fn gen_random() -> Self {
        let mut rng = thread_rng();
        Self(
            (0..24)
                .map(|_| {
                    char::from_u32(match rng.gen_range(0..3) {
                        0 => rng.gen_range(48..=57),  // Numbers
                        1 => rng.gen_range(65..=90),  // Uppercase
                        _ => rng.gen_range(97..=122), // Lowercase
                    })
                    .unwrap()
                })
                .collect::<String>(),
        )
    }
}

impl std::fmt::Debug for Password {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("*****")
    }
}

impl TryFrom<String> for Password {
    type Error = &'static str;

    fn try_from(x: String) -> Result<Password, Self::Error> {
        if x.len() < 8 {
            return Err("Password must be greater than 8 characters");
        }

        Ok(Password(x))
    }
}

impl Password {
    pub fn secret(&self) -> &str {
        &self.0
    }
}
