use serde::Deserialize;

#[derive(Deserialize)]
pub struct UpdatePasswordRequest {
    pub old_password: String,
    pub new_password: String,
    pub confirm_new_password: String,
}
