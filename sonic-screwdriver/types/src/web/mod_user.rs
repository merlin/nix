use serde::Deserialize;

#[derive(Deserialize)]
pub struct UpdateProfileRequest {
    pub firstname: Option<String>,
    pub lastname: Option<String>,
}
