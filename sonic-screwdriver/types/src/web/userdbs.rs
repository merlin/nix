use serde::{Deserialize, Serialize};

use crate::Password;

#[derive(Serialize, Deserialize)]
pub struct ManageUserDBRequest {
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserDBConnectionDetails {
    pub name: String,
    pub hostname: String,
    pub port: u16,
    pub database: String,
    pub username: String,
    pub password: Password,
}
