use std::{env, sync::Arc};

use axum::{http::StatusCode, response::Response};
use sonic_application::config::Config;
use tera::Tera;
use tokio::sync::OnceCell;

pub static TERA: OnceCell<Arc<Tera>> = OnceCell::const_new();

pub fn configure(config: &Config) {
    let templates_dir =
        env::var("SONIC_TEMPLATE_DIR").unwrap_or_else(|_| config.http.template_dir.clone());

    TERA.set(Arc::new(
        Tera::new(&format!("{}/**/*", templates_dir)).unwrap(),
    ))
    .unwrap();
}

pub fn render(templ: &'static str, ctx: tera::Context) -> Result<Response<String>, StatusCode> {
    match TERA.get().unwrap().render(templ, &ctx) {
        Ok(s) => Ok(Response::builder()
            .status(StatusCode::OK)
            .header("Content-Type", "text/html")
            .body(s)
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?),
        Err(e) => panic!("Error rendering template: {}", e),
    }
}

#[macro_export]
macro_rules! context {
    ($($key:ident: $value:expr),*$(,)?) => {
        #[allow(unused_mut, redundant_semicolons)]
        {
            let mut ctx = tera::Context::new();

            $(ctx.insert(stringify!($key), &$value););*

            ctx
        }
    };
}
