use std::collections::HashMap;

use serde::{ser::SerializeMap, Serialize};
use sonic_types::{endpoint::Endpoint, gitlab::GitlabPage};

// So apparently there is not a way to do this that doesn't involve some sort of seperate structs implementing Serialize.
// It seems stupid, and I might be wrong, but this is the best way for now.
#[derive(Serialize)]
pub struct TraefikConfig {
    http: HttpConfig,
}

impl TraefikConfig {
    pub fn merge(self, other: Self) -> Self {
        Self {
            http: self.http.merge(other.http),
        }
    }
}

#[derive(Serialize)]
struct HttpConfig {
    routers: HashMap<String, RouterConfig>,
    services: HashMap<String, ServiceConfig>,
    middlewares: HashMap<String, MiddlewareConfig>,
    #[serde(rename = "serversTransports")]
    servers_transports: HashMap<String, ServersTransportConfig>,
}

impl HttpConfig {
    pub fn merge(mut self, other: Self) -> Self {
        self.routers.extend(other.routers);
        self.services.extend(other.services);
        self.middlewares.extend(other.middlewares);
        self.servers_transports.extend(other.servers_transports);

        self
    }
}

#[derive(Debug, Serialize)]
struct RouterConfig {
    service: String,
    rule: String,
    #[serde(rename = "entryPoints")]
    entry_points: Vec<String>,
    middlewares: Vec<String>,
    tls: Option<TLSConfig>,
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
enum MiddlewareConfig {
    StripPrefix {
        #[serde(rename = "stripPrefix")]
        strip_prefix: StripPrefixConfig,
    },
    RewriteHost {
        headers: HeadersMiddleware,
    },
}

#[derive(Debug)]
struct StripPrefixConfig {
    prefixes: Vec<String>,
}

#[derive(Debug, Serialize)]
struct HeadersMiddleware {
    #[serde(rename = "customRequestHeaders")]
    custom_request_headers: HashMap<String, String>,
}

#[derive(Debug, Serialize)]
struct ServiceConfig {
    #[serde(rename = "loadBalancer")]
    load_balancer: LoadBalancerConfig,
}

#[derive(Debug, Serialize)]
struct LoadBalancerConfig {
    servers: Vec<ServerConfig>,
    #[serde(rename = "serversTransport", skip_serializing_if = "Option::is_none")]
    servers_transport: Option<String>,
}

#[derive(Debug, Serialize)]
struct ServerConfig {
    url: String,
}

#[derive(Debug, Serialize)]
struct ServersTransportConfig {
    #[serde(rename = "insecureSkipVerify")]
    insecure_skip_verify: bool,
}

#[derive(Debug, Serialize)]
struct TLSConfig {
    #[serde(rename = "certResolver")]
    cert_resolver: String,
}

fn sanitize_string(s: String) -> String {
    s.chars()
        .map(|c| if c.is_alphanumeric() { c } else { '-' })
        .collect()
}

impl From<Vec<Endpoint>> for TraefikConfig {
    fn from(v: Vec<Endpoint>) -> Self {
        let routers = v
            .iter()
            .map(|e| (RouterConfig::name(e), RouterConfig::gen(e)))
            .collect();

        let mut middlewares = HashMap::new();
        for e in v.iter() {
            if e.strip_prefix && &*e.prefix != "/" {
                middlewares.insert(
                    MiddlewareConfig::name_strip(e),
                    MiddlewareConfig::gen_strip(e),
                );
            }

            if let Some(host) = e.host_override.as_deref() {
                middlewares.insert(
                    MiddlewareConfig::name_host(e),
                    MiddlewareConfig::gen_host(host),
                );
            }
        }

        let services = v
            .iter()
            .map(|e| (ServiceConfig::name(e), ServiceConfig::gen(e)))
            .collect();

        TraefikConfig {
            http: HttpConfig {
                routers,
                middlewares,
                services,
                servers_transports: HashMap::new(),
            },
        }
    }
}

impl From<&Vec<GitlabPage>> for TraefikConfig {
    fn from(value: &Vec<GitlabPage>) -> Self {
        let routers = value
            .iter()
            .map(|p| (RouterConfig::name_gitlab(p), RouterConfig::gen_gitlab(p)))
            .collect();

        let mut services = HashMap::new();
        services.insert(
            "gitlab-pages".to_string(),
            ServiceConfig {
                load_balancer: LoadBalancerConfig {
                    servers: vec![ServerConfig {
                        url: "https://gitlab.internal.tardisproject.uk:9443".to_string(),
                    }],
                    servers_transport: Some("insecure-https".to_string()),
                },
            },
        );

        let mut servers_transports = HashMap::new();
        servers_transports.insert(
            "insecure-https".to_string(),
            ServersTransportConfig {
                insecure_skip_verify: true,
            },
        );

        TraefikConfig {
            http: HttpConfig {
                routers,
                middlewares: HashMap::new(),
                services,
                servers_transports,
            },
        }
    }
}

impl RouterConfig {
    fn get_rule(e: &Endpoint) -> String {
        format!(
            "Host(`{}`) && PathPrefix(`{}`)",
            e.domain.domain,
            &*e.prefix.replace('`', "")
        )
    }

    fn gen(e: &Endpoint) -> Self {
        let mut middlewares = vec![];
        if e.strip_prefix && &*e.prefix != "/" {
            middlewares.push(MiddlewareConfig::name_strip(e));
        }

        if e.host_override
            .as_ref()
            .is_some_and(|v| !v.trim().is_empty())
        {
            middlewares.push(MiddlewareConfig::name_host(e));
        }

        let entry_points = if e.force_https {
            vec!["websecure".to_string()]
        } else {
            vec!["websecure".to_string(), "web".to_string()]
        };

        let tls = if e.domain.is_custom() {
            Some(TLSConfig {
                cert_resolver: "letsencrypt".to_string(),
            })
        } else {
            None
        };

        Self {
            service: ServiceConfig::name(e),
            rule: Self::get_rule(e),
            entry_points,
            middlewares,
            tls,
        }
    }

    fn name(e: &Endpoint) -> String {
        sanitize_string(format!("router-{}-{}", e.domain.domain, &*e.prefix))
    }

    fn gen_gitlab(p: &GitlabPage) -> RouterConfig {
        let rule = format!(
            "Host(`{}`) && PathPrefix(`{}`)",
            p.domain,
            &*p.prefix.replace('`', "")
        );

        RouterConfig {
            service: "gitlab-pages".to_string(),
            rule,
            entry_points: vec!["websecure".to_string()],
            middlewares: vec![],
            tls: None,
        }
    }

    fn name_gitlab(p: &GitlabPage) -> String {
        sanitize_string(format!("router-{}-{}", p.domain, &*p.prefix))
    }
}

impl MiddlewareConfig {
    fn name_strip(e: &Endpoint) -> String {
        sanitize_string(format!(
            "strip-middleware-{}-{}",
            e.domain.domain, &*e.prefix
        ))
    }

    fn name_host(e: &Endpoint) -> String {
        sanitize_string(format!(
            "host-middleware-{}-{}",
            e.domain.domain, &*e.prefix
        ))
    }

    fn gen_strip(e: &Endpoint) -> Self {
        Self::StripPrefix {
            strip_prefix: StripPrefixConfig {
                prefixes: vec![e.prefix.to_string()],
            },
        }
    }

    fn gen_host(host: &str) -> Self {
        let mut custom_request_headers = HashMap::new();
        custom_request_headers.insert("Host".to_string(), host.to_string());
        custom_request_headers.insert("X-Forwarded-Host".to_string(), "".to_string());

        Self::RewriteHost {
            headers: HeadersMiddleware {
                custom_request_headers,
            },
        }
    }
}

impl ServiceConfig {
    fn server_url(e: &Endpoint) -> String {
        if e.target_is_https {
            format!("https://{}:{}", &*e.target_ip, e.target_port)
        } else {
            format!("http://{}:{}", &*e.target_ip, e.target_port)
        }
    }

    fn gen(e: &Endpoint) -> Self {
        Self {
            load_balancer: LoadBalancerConfig {
                servers: vec![ServerConfig {
                    url: Self::server_url(e),
                }],
                servers_transport: None,
            },
        }
    }

    fn name(e: &Endpoint) -> String {
        sanitize_string(format!("service-{}-{}", e.domain.domain, &*e.prefix))
    }
}

impl Serialize for StripPrefixConfig {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(2))?;
        map.serialize_entry("forceSlash", &false)?;
        map.serialize_entry("prefixes", &self.prefixes)?;

        map.end()
    }
}
