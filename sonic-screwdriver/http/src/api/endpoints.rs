use std::sync::Arc;

use crate::{
    state::Router,
    utils::{ErrorResponses, JSONError},
    web::auth::UserInfo,
};
use axum::{extract::State, routing::get, Json};
use reqwest::StatusCode;
use sonic_application::App;
use sonic_types::{
    endpoint::NewEndpoint,
    web::endpoints::{APICreateEndpointRequest, DeleteEndpointRequest, ListEndpointsResponse},
};

pub fn routes() -> Router {
    Router::new().route("/", get(list).post(create).delete(delete))
}

async fn list(
    State(app): State<Arc<App>>,
    user: UserInfo,
) -> Result<Json<ListEndpointsResponse>, JSONError> {
    let endpoints = app
        .endpoints_for_user(&user.uid)
        .await
        .err_to_json(StatusCode::INTERNAL_SERVER_ERROR)?;
    let domains = app
        .domains_for_user(&user.uid)
        .await
        .err_to_json(StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(Json(ListEndpointsResponse { endpoints, domains }))
}

async fn create(
    State(app): State<Arc<App>>,
    user: UserInfo,
    Json(endpoint): Json<APICreateEndpointRequest>,
) -> Result<Json<NewEndpoint>, JSONError> {
    let endpoint = endpoint.try_into().err_to_json(StatusCode::BAD_REQUEST)?;

    app.add_endpoint(&user.uid, &endpoint)
        .await
        .err_to_json(StatusCode::BAD_REQUEST)?;

    Ok(Json(endpoint))
}

async fn delete(
    State(app): State<Arc<App>>,
    user: UserInfo,
    Json(endpoint): Json<DeleteEndpointRequest>,
) -> Result<StatusCode, JSONError> {
    app.delete_endpoint(&user.uid, &endpoint.prefix, &endpoint.domain)
        .await
        .err_to_json(StatusCode::BAD_REQUEST)?;

    Ok(StatusCode::OK)
}
