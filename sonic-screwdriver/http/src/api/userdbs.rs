use std::sync::Arc;

use anyhow::Context;
use axum::{
    extract::State,
    http::StatusCode,
    routing::{get, post},
    Json,
};
use sonic_application::App;
use sonic_types::{
    userdb::UserDB,
    web::userdbs::{ManageUserDBRequest, UserDBConnectionDetails},
};

use crate::{
    state::Router,
    utils::{ErrorResponses, JSONError},
    web::auth::UserInfo,
};

pub fn routes() -> Router {
    Router::new()
        .route("/", get(list).post(create).delete(delete))
        .route("/reset", post(reset))
}

async fn list(State(app): State<Arc<App>>, user: UserInfo) -> Result<Json<Vec<UserDB>>, JSONError> {
    app.userdbs_for_user(&user.uid)
        .await
        .map(Json)
        .err_to_json(StatusCode::INTERNAL_SERVER_ERROR)
}

async fn create(
    State(app): State<Arc<App>>,
    user: UserInfo,
    Json(userdb): Json<ManageUserDBRequest>,
) -> Result<Json<UserDBConnectionDetails>, JSONError> {
    let name = userdb
        .name
        .try_into()
        .context("Error in database name")
        .err_to_json(StatusCode::BAD_REQUEST)?;

    let db = UserDB {
        owner: user.uid.clone(),
        name,
    };

    let password = app
        .add_userdb(&db)
        .await
        .err_to_json(StatusCode::BAD_REQUEST)?;

    Ok(Json(UserDBConnectionDetails {
        name: db.name.to_string(),
        hostname: "userdb.tardisproject.uk".to_string(),
        port: 3306,
        database: db.slug(),
        username: db.slug(),
        password,
    }))
}

async fn reset(
    State(app): State<Arc<App>>,
    user: UserInfo,
    Json(userdb): Json<ManageUserDBRequest>,
) -> Result<Json<UserDBConnectionDetails>, JSONError> {
    let name = userdb
        .name
        .try_into()
        .context("Error in database name")
        .err_to_json(StatusCode::BAD_REQUEST)?;

    let db = UserDB {
        owner: user.uid.clone(),
        name,
    };

    let password = app
        .reset_userdb_password(&db)
        .await
        .err_to_json(StatusCode::BAD_REQUEST)?;

    Ok(Json(UserDBConnectionDetails {
        name: db.name.to_string(),
        hostname: "userdb.tardisproject.uk".to_string(),
        port: 3306,
        database: db.slug(),
        username: db.slug(),
        password,
    }))
}

async fn delete(
    State(app): State<Arc<App>>,
    user: UserInfo,
    Json(userdb): Json<ManageUserDBRequest>,
) -> Result<StatusCode, JSONError> {
    let name = userdb
        .name
        .try_into()
        .context("Error in database name")
        .err_to_json(StatusCode::BAD_REQUEST)?;

    app.delete_userdb(
        &(UserDB {
            owner: user.uid.clone(),
            name,
        }),
    )
    .await
    .err_to_json(StatusCode::BAD_REQUEST)?;

    Ok(StatusCode::ACCEPTED)
}
