use crate::state::Router;

mod endpoints;
mod userdbs;

pub fn routes() -> Router {
    Router::new()
        .nest("/userdbs", userdbs::routes())
        .nest("/endpoints", endpoints::routes())
}
