use std::sync::Arc;

use axum::{
    extract::{Path, State},
    response::IntoResponse,
    routing::{get, post},
    Form,
};
use axum_extra::extract::CookieJar;
use axum_flash::IncomingFlashes;
use rand::{distributions::Alphanumeric, Rng};
use sonic_application::App;
use sonic_types::{application::PatchApplication, web::apply::ApplyRequest};

use crate::{
    context,
    flash::{make_flash, IncomingFlashesExt},
    state::Router,
    template::render,
};

use super::captcha::{CaptchaStore, IssueNewCaptcha};

pub fn routes() -> Router {
    Router::new()
        .route("/apply", get(application_form))
        .route("/apply", post(submit_application))
        .route("/verify/:token", get(verify_application))
}

async fn application_form(
    new_captcha: IssueNewCaptcha,
    flashes: IncomingFlashes,
) -> impl IntoResponse {
    (
        new_captcha,
        render(
            "apply.html",
            context! {
                captcha_suffix: gen_captcha_suffix(),
                msgs: flashes.for_template()
            },
        ),
    )
}

async fn submit_application(
    cookies: CookieJar,
    new_captcha: IssueNewCaptcha,
    State(captcha): State<Arc<CaptchaStore>>,
    State(app): State<Arc<App>>,
    Form(application): Form<ApplyRequest>,
) -> Result<impl IntoResponse, (IssueNewCaptcha, impl IntoResponse)> {
    if application.captcha_answer.is_none()
        || !captcha
            .verify_captcha(&cookies, application.captcha_answer.as_ref().unwrap())
            .await
    {
        return Err((
            new_captcha,
            render(
                "apply.html",
                context! {
                    captcha_suffix: gen_captcha_suffix(),
                    msgs: make_flash("error", "Invalid Captcha"),
                    application: application
                },
            ),
        ));
    }

    let patch_application: PatchApplication = match application.clone().try_into() {
        Ok(x) => x,
        Err(e) => {
            return Err((
                new_captcha,
                render(
                    "apply.html",
                    context! {
                        captcha_suffix: gen_captcha_suffix(),
                        msgs: make_flash("error", &format!("{:#}", e)),
                        application: application
                    },
                ),
            ))
        }
    };

    if let Err(e) = app.submit_application(patch_application).await {
        return Err((
            new_captcha,
            render(
                "apply.html",
                context! {
                    captcha_suffix: gen_captcha_suffix(),
                    msgs: make_flash("error", &format!("{:#}", e)),
                    application: application
                },
            ),
        ));
    }

    Ok(render(
        "apply_verify.html",
        context! {
            email: application.email
        },
    ))
}

async fn verify_application(
    State(app): State<Arc<App>>,
    Path(token): Path<String>,
) -> Result<impl IntoResponse, impl IntoResponse> {
    match app.verify_application(token).await {
        Ok(()) => render(
            "apply_thanks.html",
            context! {
                success: true
            },
        ),
        Err(e) => render(
            "apply_thanks.html",
            context! {
                success: false,
                error: e.to_string()
            },
        ),
    }
}

fn gen_captcha_suffix() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(20)
        .map(char::from)
        .collect()
}
