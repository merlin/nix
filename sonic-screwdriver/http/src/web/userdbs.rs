use std::sync::Arc;

use crate::{
    context, flash::IncomingFlashesExt, state::Router, template::render, utils::ErrorResponses,
};
use anyhow::Context;
use axum::{
    extract::State,
    response::{IntoResponse, Redirect},
    routing::{get, post},
    Form,
};
use axum_flash::{Flash, IncomingFlashes};
use sonic_application::App;
use sonic_types::{userdb::UserDB, web::userdbs::ManageUserDBRequest, Password};

use super::auth::UserInfo;

pub fn routes() -> Router {
    Router::new()
        .route("/", get(list).post(create))
        .route("/reset", post(reset))
        .route("/delete", post(delete))
}

async fn list(
    State(app): State<Arc<App>>,
    flashes: IncomingFlashes,
    flash: Flash,
    user: UserInfo,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let userdbs = app
        .userdbs_for_user(&user.uid)
        .await
        .err_to_redirect(&flash, "/")?;

    let templ = render(
        "userdbs.html",
        context! {
            msgs: flashes.for_template(),
            userdbs: userdbs
        },
    );
    Ok((flash, templ))
}

async fn create(
    State(app): State<Arc<App>>,
    flash: Flash,
    user: UserInfo,
    Form(userdb): Form<ManageUserDBRequest>,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let name = userdb
        .name
        .try_into()
        .context("Error in database name")
        .err_to_redirect(&flash, "/userdbs")?;
    let db = UserDB {
        owner: user.uid.clone(),
        name,
    };

    let password = app
        .add_userdb(&db)
        .await
        .err_to_redirect(&flash, "/userdbs")?;

    Ok((
        flash.success(format!(
            "Successfully added user database! {}",
            gen_connection_details(&db, &password)
        )),
        Redirect::to("/userdbs"),
    ))
}

async fn reset(
    State(app): State<Arc<App>>,
    flash: Flash,
    user: UserInfo,
    Form(userdb): Form<ManageUserDBRequest>,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let name = userdb
        .name
        .try_into()
        .context("Error in database name")
        .err_to_redirect(&flash, "/userdbs")?;
    let db = UserDB {
        owner: user.uid.clone(),
        name,
    };

    let password = app
        .reset_userdb_password(&db)
        .await
        .err_to_redirect(&flash, "/userdbs")?;

    Ok((
        flash.success(format!(
            "Successfully reset user database password! {}",
            gen_connection_details(&db, &password)
        )),
        Redirect::to("/userdbs"),
    ))
}

async fn delete(
    State(app): State<Arc<App>>,
    flash: Flash,
    user: UserInfo,
    Form(userdb): Form<ManageUserDBRequest>,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let name = userdb
        .name
        .try_into()
        .context("Error in database name")
        .err_to_redirect(&flash, "/userdbs")?;

    app.delete_userdb(
        &(UserDB {
            owner: user.uid.clone(),
            name,
        }),
    )
    .await
    .err_to_redirect(&flash, "/userdbs")?;

    Ok((
        flash.success("Database deleted successfully."),
        Redirect::to("/userdbs"),
    ))
}

fn gen_connection_details(db: &UserDB, password: &Password) -> String {
    format!(
        "Connect with the following details: <br>\
         Hostname: <code>userdbs.tardisproject.uk</code> <br>\
         Port: <code>3306</code>
         Database: <code>{}</code> <br>\
         Username: <code>{}</code> <br>\
         Password: <code>{}</code> <br>\
         <strong>This password won't be shown again, so record it somewhere.</strong>",
        db.slug(),
        db.slug(),
        password.secret()
    )
}
