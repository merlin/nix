use std::sync::Arc;

use crate::{
    context, flash::IncomingFlashesExt, state::Router, template::render, traefik::TraefikConfig,
    utils::ErrorResponses,
};
use axum::{
    extract::State,
    response::{IntoResponse, Redirect},
    routing::{get, post},
    Form, Json,
};
use axum_flash::{Flash, IncomingFlashes};
use sonic_application::App;
use sonic_types::web::endpoints::{CreateEndpointRequest, DeleteEndpointRequest};

use super::auth::UserInfo;

pub fn routes() -> Router {
    Router::new()
        .route("/endpoints", get(list).post(create))
        .route("/endpoints/delete", post(delete))
        .route("/traefik", get(traefik))
}

async fn list(
    State(app): State<Arc<App>>,
    flashes: IncomingFlashes,
    flash: Flash,
    user: UserInfo,
) -> Result<impl IntoResponse, (Flash, Redirect)> {
    let endpoints = app
        .endpoints_for_user(&user.uid)
        .await
        .err_to_redirect(&flash, "/")?;
    let domains = app
        .domains_for_user(&user.uid)
        .await
        .err_to_redirect(&flash, "/")?;

    let templ = render(
        "endpoints.html",
        context! {
            msgs: flashes.for_template(),
            endpoints: endpoints,
            domains: domains,
        },
    );
    Ok((flash, templ))
}

async fn create(
    State(app): State<Arc<App>>,
    flash: Flash,
    user: UserInfo,
    Form(endpoint): Form<CreateEndpointRequest>,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    let endpoint = endpoint.try_into().err_to_redirect(&flash, "/endpoints")?;

    app.add_endpoint(&user.uid, &endpoint)
        .await
        .err_to_redirect(&flash, "/endpoints")?;

    Ok((
        flash.success("Successfully added endpoint! This may take up to 5 minutes to apply."),
        Redirect::to("/endpoints"),
    ))
}

async fn delete(
    State(app): State<Arc<App>>,
    flash: Flash,
    user: UserInfo,
    Form(endpoint): Form<DeleteEndpointRequest>,
) -> Result<(Flash, Redirect), (Flash, Redirect)> {
    app.delete_endpoint(&user.uid, &endpoint.prefix, &endpoint.domain)
        .await
        .err_to_redirect(&flash, "/endpoints")?;

    Ok((
        flash.success("Endpoint deleted successfully. This may take up to 5 minutes to apply."),
        Redirect::to("/endpoints"),
    ))
}

async fn traefik(State(app): State<Arc<App>>) -> Result<Json<TraefikConfig>, &'static str> {
    let endpoints: TraefikConfig = app
        .get_all_endpoints()
        .await
        .map_err(|_| "Internal Error")?
        .into();
    let pages = app.get_gitlab_pages().await.map_err(|_| "Internal Error")?;
    let pages: TraefikConfig = (&*pages).into();

    Ok(Json(endpoints.merge(pages)))
}
