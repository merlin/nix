use axum::{response::IntoResponse, routing::get};
use axum_flash::IncomingFlashes;

use crate::{context, flash::IncomingFlashesExt, template::render, Router};

use self::auth::UserInfo;

pub mod applications;
pub mod apply;
pub mod auth;
pub mod captcha;
pub mod change_password;
pub mod custom_domains;
pub mod endpoints;
pub mod k8s;
pub mod logout;
pub mod mod_user;
pub mod pw_reset;
pub mod userdbs;

pub fn routes() -> Router {
    Router::new()
        .merge(auth::routes())
        .merge(captcha::routes())
        .merge(apply::routes())
        .merge(applications::routes())
        .nest("/k8s", k8s::routes())
        .nest("/profile", mod_user::routes())
        .nest("/password", change_password::routes())
        .nest("/userdbs", userdbs::routes())
        .nest("/logout", logout::routes())
        .merge(endpoints::routes())
        .merge(pw_reset::routes())
        .merge(custom_domains::routes())
        .route("/", get(index))
}

async fn index(user: UserInfo, flashes: IncomingFlashes) -> impl IntoResponse {
    let templ = render(
        "index.html",
        context! {
            user: user,
            msgs: flashes.for_template(),
        },
    );

    (flashes, templ)
}
