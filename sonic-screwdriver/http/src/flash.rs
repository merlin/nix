use axum_flash::Level;
use serde::Serialize;

pub trait IncomingFlashesExt {
    fn for_template(&self) -> Option<Vec<FlashMessage<'_>>>;
}

#[derive(Debug, Serialize)]
pub struct FlashMessage<'a> {
    kind: &'static str,
    text: &'a str,
}

pub fn make_flash<'a>(kind: &'static str, text: &'a str) -> Vec<FlashMessage<'a>> {
    vec![FlashMessage { kind, text }]
}

impl IncomingFlashesExt for axum_flash::IncomingFlashes {
    fn for_template(&self) -> Option<Vec<FlashMessage<'_>>> {
        if self.is_empty() {
            None
        } else {
            Some(
                self.iter()
                    .map(|(kind, text)| FlashMessage {
                        kind: match kind {
                            Level::Debug => "debug",
                            Level::Info => "info",
                            Level::Success => "success",
                            Level::Warning => "warning",
                            Level::Error => "error",
                        },
                        text,
                    })
                    .collect(),
            )
        }
    }
}
