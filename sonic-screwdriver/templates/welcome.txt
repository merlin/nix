Hi {{ first_name }},

You're receiving this email because you recently applied for a Tardis account.
We've now processed your application, and created your account! You can log in with the details below.

  Username: {{ username }}
  Password: {{ password }}

Please change this password after logging in for the first time, using either our web interface at https://console.tardisproject.uk', or kpasswd' on the shell

Now you can:

  * Access our servers remotely - https://wiki.tardisproject.uk/tutorials:start
  * Check your tardisproject.uk email account - https://mail.tardisproject.uk 
  * Collaborate on code with Gitlab, and make a personal homepage -  https://git.tardisproject.uk 
  * Deploy things on our Kubernetes cluster - https://k8s.tardisproject.uk 
  * Use our cloud storage for your files - https://cloud.tardisproject.uk 

and more! Check our tour ( https://wiki.tardisproject.uk/meta:tour ) or our service homepage ( https://tard.is ) for a full list of everything you have access to.

Also remember to join our chat channel ( https://tardisproject.uk/chat ), and please don't hesitate to reach out if you have any questions or concerns.

Sincerely,
Tardis Admins.
