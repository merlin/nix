# sonic-screwdriver

This contains code for `sonic-screwdriver`, our tool for common user/admin tasks. Currently this handles:

  - Applications and adding users
  - Password and name changing for users
  - Managing user k8s namespaces
  - Managing user databases
  - Managing user endpoints

It is available as a web interface, and as a more limited CLI tool.

## Architecture

We organise this project using [hexagonal architecture](https://en.wikipedia.org/wiki/Hexagonal_architecture_(software)). In summary,

  * Ports, currently our HTTP interface and CLI, which take in requests
  * Domain logic, which includes validation and the logical steps we need to take
  * Adapters, which interface with databases and other APIs.

The first is stored in the `http` crate, while the last two are split across type `types` and  `application` crates.
The `App` struct and the `types` crate contains all of the domain logic, while the `application::adapters` module contains the adapters.

There are some other conventions we use specific to this project:

  * If an error shouldn't be shown to the user, it should not leave the domain logic (`application::application::*`). See `application/src/application/errors.rs` for one way to achieve this.
  * We prefer new-typing where possible, to ensure validation has been done at the type-system level. See `application/src/entities/username.rs` for an example.

## Development

You can use the provided `docker-compose.yml` to get a local openldap, KDC server, and mailhog (mock smtp) up to test against.

Since the change password commands use your system kerberos client, you'll need to modify `/etc/krb5.conf` to something like this:

```
[libdefaults]
    default_realm = TARDISPROJECT.UK
    dns_lookup_kdc = false
[defaults]
    dns_lookup_kdc = false

[realms]
    TARDISPROJECT.UK = {
        kdc = localhost
        admin_server = localhost
        master_kdc = localhost
        default_domain = tardisproject.uk
    }

[domain_realm]
    tardisproject.uk = TARDISPROJECT.UK
```

You can see what emails the app is sending at `localhost:8025`, connect to LDAP on `localhost:1389`, and use `docker compose exec kdc kadmin.local` to view the kerberos database.

Alternatively, you can use `./krb_local.sh` to forward your local kerberos port to the actual Tardis kerberos server over SSH. This can make it easier to test the CLI.
