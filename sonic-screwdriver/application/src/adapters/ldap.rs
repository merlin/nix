use std::{
    cmp::max,
    collections::{HashMap, HashSet},
    hash::Hash,
};

use crate::config::Ldap as LdapConfig;
use anyhow::{Context, Result};
use ldap3::{Ldap, LdapConnAsync, Mod, Scope, SearchEntry};
use lettre::Address as EmailAddress;
use sonic_types::{User, Username};

pub struct LdapAdapter {
    conf: LdapConfig,
}

impl LdapAdapter {
    pub async fn new(conf: &LdapConfig) -> Result<Self> {
        Ok(Self { conf: conf.clone() })
    }

    async fn ldap(&self) -> Result<Ldap> {
        let (conn, mut ldap) =
            LdapConnAsync::new(&format!("ldap://{}:{}", &self.conf.host, self.conf.port)).await?;
        ldap3::drive!(conn);

        ldap.simple_bind(&self.conf.bind_dn, &self.conf.bind_pw)
            .await
            .context("error authenticating to ldap server")?;

        Ok(ldap)
    }

    /// Get the next uid to use
    pub async fn next_uid(&self) -> Result<usize> {
        let highest_uid = self
            .max_attr("uidNumber")
            .await
            .context("error getting max uid")?;

        Ok(highest_uid + 1)
    }

    /// Get the maximum value of the given attribute in the user subtree
    async fn max_attr(&self, attr: &str) -> Result<usize> {
        let (search_result, _) = self
            .ldap()
            .await?
            .search(
                &self.conf.users_base,
                Scope::Subtree,
                "(objectClass=posixAccount)",
                vec![attr],
            )
            .await
            .and_then(|x| x.success())
            .context("error performing ldap search")?;

        let mut max_so_far = 1000_usize;
        for entry in search_result {
            max_so_far = max(
                max_so_far,
                SearchEntry::construct(entry)
                    .attrs
                    .into_iter()
                    .flat_map(|(_, vs)| vs)
                    .filter_map(|x| x.parse::<usize>().ok())
                    .max()
                    .unwrap_or(1000),
            );
        }

        Ok(max_so_far)
    }

    pub async fn add_user(&self, user: &User) -> Result<LdapRecord> {
        let uid = self.next_uid().await?;
        let (dn, obj) = self.gen_user_obj(user, uid);

        self.ldap()
            .await?
            .add(&dn, obj.clone().into())
            .await
            .and_then(|x| x.success())
            .context("error adding user to ldap")?;

        Ok(obj)
    }

    fn gen_user_obj(&self, user: &User, uid: usize) -> (String, LdapRecord) {
        let dn = format!(
            "uid={},ou={},{}",
            user.username(),
            if matches!(user, User::System { .. }) {
                "services"
            } else {
                "people"
            },
            &self.conf.users_base
        );
        let mut attrs = LdapRecord::default();

        attrs.add("objectClass".into(), "inetOrgPerson".into());
        attrs.add("objectClass".into(), "organizationalPerson".into());
        attrs.add("objectClass".into(), "person".into());
        attrs.add("objectClass".into(), "top".into());
        attrs.add("objectClass".into(), "posixAccount".into());

        attrs.add("uid".into(), user.username().to_string());
        match user {
            User::Person {
                username,
                first_name,
                last_name,
                external_email,
                graduation_year,
                is_legacy_user,
                sponsor,
            } => {
                attrs.add("objectClass".into(), "tardisUser".into());

                attrs.add("cn".into(), first_name.clone());
                attrs.add("sn".into(), last_name.clone());

                attrs.add("gidNumber".into(), 1001.to_string()); // users group

                // Tardis specific attributes
                attrs.add("externalEmail".into(), external_email.to_string());
                if let Some(graduation_year) = graduation_year {
                    attrs.add("graduationYear".into(), graduation_year.to_string());
                }

                attrs.add(
                    "isLegacyUser".into(),
                    is_legacy_user.to_string().to_uppercase(),
                );

                if let Some(sponsor) = sponsor {
                    attrs.add("sponsor".into(), sponsor.clone());
                }

                attrs.add("homeDirectory".into(), format!("/home/{}", username));
            }
            User::System { username } => {
                attrs.add("cn".into(), username.to_string());
                attrs.add("sn".into(), "System Account".into());

                attrs.add("gidNumber".into(), 1337.to_string()); // system users group

                attrs.add("loginShell".into(), "/sbin/nologin".into());
                attrs.add("homeDirectory".into(), "/var/empty".into());
            }
        };

        attrs.add("uidNumber".into(), uid.to_string());

        attrs.add(
            "mail".into(),
            format!("{}@tardisproject.uk", user.username()),
        );

        // Passthrough authentication to kerberos
        attrs.add(
            "userPassword".into(),
            format!("{{SASL}}{}@TARDISPROJECT.UK", user.username()),
        );

        (dn, attrs)
    }

    pub async fn find_user_dn(&self, user: &Username) -> Result<Option<String>> {
        let search_result = self
            .ldap()
            .await?
            .search(
                &self.conf.users_base,
                Scope::Subtree,
                &format!("(uid={})", user),
                vec!["dn"],
            )
            .await?;
        search_result.1.success()?;
        let mut users = search_result.0;
        if users.is_empty() {
            return Ok(None);
        }

        let user = SearchEntry::construct(users.pop().unwrap());

        Ok(Some(user.dn))
    }

    pub async fn uid_from_email(&self, email: &EmailAddress) -> Result<Option<String>> {
        let search_result = self
            .ldap()
            .await?
            .search(
                &self.conf.users_base,
                Scope::Subtree,
                &format!("(externalEmail={})", email),
                vec!["uid"],
            )
            .await?;
        search_result.1.success()?;
        let mut users = search_result.0;
        if users.is_empty() {
            return Ok(None);
        }

        let user = SearchEntry::construct(users.pop().unwrap());

        Ok(dbg!(user.attrs).remove("uid").map(|mut vs| vs.remove(0)))
    }

    pub async fn apply_modifications<S: Hash + Eq + AsRef<[u8]>>(
        &self,
        dn: &str,
        mods: Vec<Mod<S>>,
    ) -> Result<()> {
        let result = self
            .ldap()
            .await?
            .modify(dn, mods)
            .await
            .context("error communicating with ldap server")?;

        result
            .success()
            .context("error response from ldap server")?;

        Ok(())
    }

    pub async fn username_taken(&self, user: &Username) -> Result<bool> {
        Ok(self.find_user_dn(user).await?.is_some())
    }
}

#[derive(Default, Debug, Clone)]
pub struct LdapRecord(HashMap<String, HashSet<String>>);

impl LdapRecord {
    pub fn add(&mut self, name: String, val: String) {
        self.0.entry(name).or_default().insert(val);
    }
}

impl From<LdapRecord> for Vec<(String, HashSet<String>)> {
    fn from(val: LdapRecord) -> Self {
        val.0.into_iter().collect()
    }
}
