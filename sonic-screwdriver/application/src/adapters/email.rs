use std::sync::Arc;

use anyhow::{anyhow, Result};
use lettre::{
    message::{Mailbox, MultiPart},
    transport::smtp::{authentication::Credentials, extension::ClientId},
    Address as EmailAddress, AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor,
};
use tera::Tera;

use crate::config::Smtp as SmtpConfig;
use sonic_types::{application::PatchApplication, Password, User};

pub struct EmailsAdapter {
    smtp: OurSmtpTransport,
    templates: Arc<Tera>,
}

impl EmailsAdapter {
    pub fn new(config: &SmtpConfig, templates: Arc<Tera>) -> Self {
        let smtp = match (config.starttls, config.ssl) {
            (true, false) => {
                AsyncSmtpTransport::<Tokio1Executor>::starttls_relay(&config.host).unwrap()
            }
            (false, true) => AsyncSmtpTransport::<Tokio1Executor>::relay(&config.host).unwrap(),
            (false, false) => AsyncSmtpTransport::<Tokio1Executor>::builder_dangerous(&config.host),
            _ => panic!("Exactly one or zero of smtp.starttls and smtp.ssl must be set"),
        }
        .port(config.port)
        .hello_name(ClientId::Domain("enclave.tardisproject.uk".to_owned()))
        .credentials(Credentials::new(
            config.username.clone(),
            config.password.clone(),
        ))
        .build();

        Self { smtp, templates }
    }

    pub async fn send_welcome_email(&self, user: &User, password: &Password) -> Result<()> {
        // Send email
        match user {
            User::System { .. } => Err(anyhow!("Attempt to send welcome email to system user")),
            User::Person {
                username,
                first_name,
                last_name,
                external_email,
                ..
            } => {
                let full_name = format!("{} {}", first_name, last_name);
                let tardis_email = format!("{}@tardisproject.uk", username);
                let ctx = {
                    let mut ctx = tera::Context::new();
                    ctx.insert("first_name", first_name);
                    ctx.insert("username", &**username);
                    ctx.insert("password", password.secret());
                    ctx
                };
                let email = Message::builder()
                    .from(Mailbox::new(
                        Some("Tardis Admins".to_string()),
                        "console@tardisproject.uk".parse().unwrap(),
                    ))
                    .reply_to(Mailbox::new(
                        Some("Tardis Admins".to_string()),
                        "hello@tardisproject.uk".parse().unwrap(),
                    ))
                    .to(Mailbox::new(
                        Some(full_name.clone()),
                        external_email.clone(),
                    ))
                    .to(Mailbox::new(Some(full_name), tardis_email.parse().unwrap()))
                    .subject("Welcome to Tardis!")
                    .multipart(MultiPart::alternative_plain_html(
                        self.templates.render("welcome.txt", &ctx).unwrap(),
                        self.templates.render("welcome.html", &ctx).unwrap(),
                    ))
                    .unwrap();

                self.smtp.send(email).await?;

                Ok(())
            }
        }
    }

    pub async fn send_application_confirm_email(
        &self,
        app: &PatchApplication,
        token: &Password,
    ) -> Result<()> {
        let full_name = format!("{} {}", &*app.first_name, &*app.last_name);

        let ctx = {
            let mut ctx = tera::Context::new();
            ctx.insert("full_name", &full_name);
            ctx.insert("username", &app.preferred_username);
            ctx.insert("token", token.secret());
            ctx
        };

        let email = Message::builder()
            .from(Mailbox::new(
                Some("Tardis Admins".to_string()),
                "console@tardisproject.uk".parse().unwrap(),
            ))
            .reply_to(Mailbox::new(
                Some("Tardis Admins".to_string()),
                "hello@tardisproject.uk".parse().unwrap(),
            ))
            .to(Mailbox::new(Some(full_name.clone()), app.email.clone()))
            .subject("Verify your tardis application")
            .multipart(MultiPart::alternative_plain_html(
                self.templates.render("verify.txt", &ctx).unwrap(),
                self.templates.render("verify.html", &ctx).unwrap(),
            ))
            .unwrap();

        self.smtp.send(email).await?;

        Ok(())
    }

    pub async fn send_reset_pw_email(&self, email: &EmailAddress, token: &Password) -> Result<()> {
        let ctx = {
            let mut ctx = tera::Context::new();
            ctx.insert("token", token.secret());
            ctx
        };

        let email = Message::builder()
            .from(Mailbox::new(
                Some("Tardis Admins".to_string()),
                "console@tardisproject.uk".parse().unwrap(),
            ))
            .reply_to(Mailbox::new(
                Some("Tardis Admins".to_string()),
                "hello@tardisproject.uk".parse().unwrap(),
            ))
            .to(Mailbox::new(None, email.clone()))
            .subject("Reset your tardis password")
            .multipart(MultiPart::alternative_plain_html(
                self.templates
                    .render("reset_password_email.txt", &ctx)
                    .unwrap(),
                self.templates
                    .render("reset_password_email.html", &ctx)
                    .unwrap(),
            ))
            .unwrap();

        self.smtp.send(email).await?;

        Ok(())
    }
}

pub type OurSmtpTransport = lettre::transport::smtp::AsyncSmtpTransport<lettre::Tokio1Executor>;
