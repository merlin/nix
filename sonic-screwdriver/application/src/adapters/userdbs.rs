use anyhow::Result;
use futures_util::TryStreamExt;
use sqlx::{query, query_as, MySqlPool};

use super::{DBError, Pool};
use sonic_types::{userdb::UserDB, Password, Username};

pub struct UserDBsAdapter {
    management_pool: Pool,
    userdb_pool: MySqlPool,
}

impl UserDBsAdapter {
    pub fn new(management_pool: Pool, userdb_pool: MySqlPool) -> Self {
        Self {
            management_pool,
            userdb_pool,
        }
    }

    pub async fn create(&self, db: &UserDB) -> Result<(), DBError> {
        query(
            "INSERT INTO userdb
            (owner, name)
            VALUES ($1, $2)",
        )
        .bind(&**db.owner)
        .bind(&*db.name)
        .execute(&self.management_pool)
        .await?;

        Ok(())
    }

    pub async fn get_by_user(&self, uid: &Username) -> Result<Vec<UserDB>> {
        let mut names = query_as::<_, (String,)>("SELECT name FROM userdb WHERE owner = $1")
            .bind(&**uid)
            .fetch(&self.management_pool);

        let mut userdbs = vec![];
        while let Some((name,)) = names.try_next().await? {
            userdbs.push(UserDB {
                name: name.try_into().expect("invalid dbname in database"),
                owner: uid.clone(),
            });
        }

        Ok(userdbs)
    }

    pub async fn delete(&self, db: &UserDB) -> Result<()> {
        query("DELETE FROM userdb WHERE owner = $1 AND name = $2")
            .bind(&*db.owner)
            .bind(&*db.name)
            .execute(&self.management_pool)
            .await?;

        Ok(())
    }

    pub async fn provision(&self, db: &UserDB) -> Result<Password> {
        match self.provision_inner(db).await {
            Ok(pass) => Ok(pass),
            Err(err) => match self.unprovision(db).await {
                Ok(_) => Err(err),
                Err(err2) => Err(err2
                    .context(err)
                    .context("Unprovisioning because of error provisioning")),
            },
        }
    }

    async fn provision_inner(&self, db: &UserDB) -> Result<Password> {
        let password = Password::gen_random();
        // No point using transactions, since creating a database auto-commits and can't be rolled back.
        query(&format!("CREATE DATABASE IF NOT EXISTS `{}`", db.slug(),))
            .execute(&self.userdb_pool)
            .await?;

        query(&format!(
            "CREATE USER IF NOT EXISTS `{}` IDENTIFIED BY '{}'",
            db.slug(),
            password.secret()
        ))
        .execute(&self.userdb_pool)
        .await?;

        query(&format!(
            "GRANT ALL ON `{}`.* TO `{}`",
            db.slug(),
            db.slug()
        ))
        .execute(&self.userdb_pool)
        .await?;

        query("FLUSH PRIVILEGES").execute(&self.userdb_pool).await?;

        Ok(password)
    }

    pub async fn unprovision(&self, db: &UserDB) -> Result<()> {
        query(&format!("DROP DATABASE `{}`", db.slug(),))
            .execute(&self.userdb_pool)
            .await?;

        query(&format!("DROP USER `{}`", db.slug()))
            .execute(&self.userdb_pool)
            .await?;

        Ok(())
    }

    pub async fn reset_password(&self, db: &UserDB) -> Result<Password> {
        let password = Password::gen_random();
        query(&format!(
            "SET PASSWORD FOR '{}' = '{}'",
            db.slug(),
            password.secret()
        ))
        .execute(&self.userdb_pool)
        .await?;

        Ok(password)
    }
}
