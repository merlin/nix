use anyhow::Result;
use sonic_types::{
    endpoint::{AddDomain, Domain},
    Username,
};
use sqlx::{query, query_as, FromRow};

use super::Pool;

pub struct DomainsAdapter {
    pool: Pool,
}

impl DomainsAdapter {
    pub fn new(pool: Pool) -> Self {
        Self { pool }
    }

    pub async fn get_all(&self) -> Result<Vec<Domain>> {
        query_as::<_, DomainRow>(
            "SELECT id, owner, domain
            FROM domain",
        )
        .fetch_all(&self.pool)
        .await
        .map(|v| v.into_iter().map(Into::into).collect())
        .map_err(Into::into)
    }

    pub async fn get_by_user(&self, owner: &str) -> Result<Vec<Domain>> {
        query_as::<_, DomainRow>(
            "SELECT id, owner, domain
            FROM domain
            WHERE owner = $1",
        )
        .bind(owner)
        .fetch_all(&self.pool)
        .await
        .map(|v| v.into_iter().map(Into::into).collect())
        .map_err(Into::into)
    }

    pub async fn belongs_to_user(&self, domain: &str, username: &Username) -> Result<bool> {
        query_as::<_, (i32,)>("SELECT 1 FROM domain WHERE domain = $1 AND owner = $2")
            .bind(domain)
            .bind(&**username)
            .fetch_optional(&self.pool)
            .await
            .map(|r| r.is_some())
            .map_err(Into::into)
    }

    pub async fn add(&self, domain: &AddDomain) -> Result<()> {
        let AddDomain { owner, domain } = domain;
        query("INSERT INTO domain (owner, domain) VALUES ($1, $2)")
            .bind(&**owner)
            .bind(domain)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    pub async fn delete(&self, domain: &AddDomain) -> Result<()> {
        let AddDomain { owner, domain } = domain;
        query("DELETE FROM domain WHERE owner = $1 AND domain = $2")
            .bind(&**owner)
            .bind(domain)
            .execute(&self.pool)
            .await?;

        Ok(())
    }
}

#[derive(Debug, FromRow)]
pub struct DomainRow {
    id: i32,
    owner: String,
    domain: String,
}

impl From<DomainRow> for Domain {
    fn from(r: DomainRow) -> Self {
        Domain {
            id: r.id,
            owner: r.owner.try_into().unwrap(),
            domain: r.domain,
        }
    }
}
