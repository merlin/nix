use std::collections::HashMap;

pub struct DiscordAdapter {
    webhook_url: String,
}

impl DiscordAdapter {
    pub fn new(webhook_url: String) -> Self {
        Self { webhook_url }
    }

    pub async fn notify_application_submit(&self) -> Result<(), reqwest::Error> {
        let mut map = HashMap::new();
        map.insert("content", "A new application was submitted");

        reqwest::Client::new()
            .post(&self.webhook_url)
            .json(&map)
            .send()
            .await?;

        Ok(())
    }
}
