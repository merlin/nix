use std::str::FromStr;

use super::{DBError, Pool};

use anyhow::Result;
use lettre::Address as EmailAddress;
use sonic_types::Password;
use sqlx::{query, query_as};

pub struct PwResetsAdapter {
    pool: Pool,
}

impl PwResetsAdapter {
    pub fn new(pool: Pool) -> Self {
        Self { pool }
    }

    pub async fn store_reset_token(&self, email: &EmailAddress, token: &Password) -> Result<()> {
        query(
            "INSERT INTO pw_reset (email, token ,expires_at) VALUES ($1, $2, datetime('now', '+1 days'))",
        )
        .bind(email.to_string())
        .bind(token.secret())
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    pub async fn get_token_email(&self, token: &Password) -> Result<EmailAddress, DBError> {
        query_as::<_, (String,)>(
            "SELECT email FROM pw_reset WHERE token = $1 AND expires_at > datetime('now')",
        )
        .bind(token.secret())
        .fetch_one(&self.pool)
        .await
        .map_err(Into::into)
        .map(|(a,)| EmailAddress::from_str(&a).unwrap())
    }

    pub async fn remove_token(&self, token: &Password) -> Result<()> {
        query("DELETE FROM pw_reset WHERE token = $1")
            .bind(token.secret())
            .execute(&self.pool)
            .await?;

        Ok(())
    }
}
