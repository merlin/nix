use std::time::{Duration, SystemTime};

use crate::App;
use anyhow::Result;
use sonic_types::gitlab::GitlabPage;
use tokio::sync::{RwLock, RwLockReadGuard};

use super::errors::HideErrorExt;

const CACHE_DURATION: Duration = Duration::from_secs(60);

impl App {
    /// Get all gitlab pages endpoints currently registered.
    /// Because gitlab's API is weird (and actually internal only), this requires n+1 queries
    /// To mitigate this, we cache it in `get_gitlab_pages`.
    /// Ideally there would be a better solution, but that would need to be implemented by gitlab probably.
    async fn do_get_gitlab_pages(&self) -> Result<Vec<GitlabPage>> {
        let users = self
            .gitlab
            .get_usernames()
            .await
            .hide_err("while fetching users from gitlab")?;

        let domains = users.into_iter().map(|u| format!("{}.tardis.ac", u));
        let mut pages = vec![];
        for domain in domains {
            pages.extend(
                self.gitlab
                    .get_pages(domain)
                    .await
                    .hide_err("while getting pages for user")?,
            );
        }

        Ok(pages)
    }

    /// Get all gitlab pages endpoints currently registered.
    /// This is cached and refreshes according to `CACHE_DURATION`
    pub async fn get_gitlab_pages(&self) -> Result<RwLockReadGuard<'_, Vec<GitlabPage>>> {
        let lock = self.gitlab_cache.read().await;
        let lock = if SystemTime::now().duration_since(lock.last_updated).unwrap() > CACHE_DURATION
        {
            drop(lock);
            {
                let mut lock = self.gitlab_cache.write().await;
                lock.data = self.do_get_gitlab_pages().await?;
                lock.last_updated = SystemTime::now();
            }

            RwLockReadGuard::map(self.gitlab_cache.read().await, |d| &d.data)
        } else {
            RwLockReadGuard::map(lock, |d| &d.data)
        };

        Ok(lock)
    }
}

pub type GitlabCache = RwLock<GitlabCacheEntry>;
pub struct GitlabCacheEntry {
    data: Vec<GitlabPage>,
    last_updated: SystemTime,
}
impl Default for GitlabCacheEntry {
    fn default() -> Self {
        Self {
            data: vec![],
            last_updated: SystemTime::UNIX_EPOCH,
        }
    }
}
