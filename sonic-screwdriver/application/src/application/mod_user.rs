use std::collections::HashSet;

use anyhow::{anyhow, Result};
use ldap3::Mod;
use sonic_types::Username;

use super::errors::HideErrorExt;
use crate::App;

impl App {
    /// Modify a user's details
    pub async fn mod_user(
        &self,
        username: &Username,
        firstname: Option<&str>,
        lastname: Option<&str>,
    ) -> Result<()> {
        if firstname.is_none() && lastname.is_none() {
            return Ok(()); // nop
        }

        // Check user exists and get dn
        let dn = self
            .ldap
            .find_user_dn(username)
            .await
            .hide_err("Error finding user dn")?
            .ok_or_else(|| anyhow!("User not found"))?;

        // Construct modification list
        let mut mods = Vec::with_capacity(2);
        if let Some(cn) = firstname {
            let mut vals = HashSet::new();
            vals.insert(cn);
            mods.push(Mod::Replace("cn", vals));
        }
        if let Some(sn) = lastname {
            let mut vals = HashSet::new();
            vals.insert(sn);
            mods.push(Mod::Replace("sn", vals));
        }

        // Do modification
        self.ldap
            .apply_modifications(&dn, mods)
            .await
            .hide_err("Error applying modifications")?;
        Ok(())
    }
}
