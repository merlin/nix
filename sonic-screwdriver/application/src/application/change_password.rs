use anyhow::Result;

use crate::App;
use sonic_types::{Password, Username};

impl App {
    /// Change a user's password, authenticating with the old one
    pub async fn change_password(
        &self,
        username: &Username,
        old_password: &Password,
        new_password: &Password,
    ) -> Result<()> {
        self.krb
            .change_password_using_old(username, old_password, new_password)
            .await
    }
}
