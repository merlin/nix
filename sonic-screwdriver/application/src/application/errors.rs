use std::fmt::Display;

use anyhow::anyhow;

/// Log the given error with the given context, and return a new generic error that hides any details
pub fn log_hidden_err(context: &'static str, e: impl Display) -> anyhow::Error {
    eprintln!("{}: {:#}", context, e);
    anyhow!("An unexpected error has occurred. Please try again, or contact an admin.")
}

/// Allows us to hide the error inside a Result, replacing it with a boilerplate text and logging it to the console with the given context.
/// This is used for potentially sensitive errors, such as database errors.
pub trait HideErrorExt<T> {
    fn hide_err(self, context: &'static str) -> Result<T, anyhow::Error>;
}
impl<T, E: Display> HideErrorExt<T> for Result<T, E> {
    fn hide_err(self, context: &'static str) -> Result<T, anyhow::Error> {
        self.map_err(|e| log_hidden_err(context, e))
    }
}
