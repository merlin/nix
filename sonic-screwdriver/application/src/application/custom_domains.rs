use anyhow::Result;

use crate::App;
use sonic_types::endpoint::{AddDomain, Domain};

use super::errors::HideErrorExt;

impl App {
    pub async fn all_domains(&self) -> Result<Vec<Domain>> {
        self.domains
            .get_all()
            .await
            .map(Into::into)
            .hide_err("Error getting all domains")
    }

    pub async fn add_domain(&self, domain: &AddDomain) -> Result<()> {
        self.domains
            .add(domain)
            .await
            .hide_err("Error adding domain")?;

        Ok(())
    }

    pub async fn delete_domain(&self, domain: &AddDomain) -> Result<()> {
        self.domains
            .delete(domain)
            .await
            .hide_err("Error deleting domain")?;

        Ok(())
    }
}
