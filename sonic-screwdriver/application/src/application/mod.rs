use std::sync::Arc;

use anyhow::Result;
use sqlx::MySqlPool;
use tera::Tera;

mod add_user;
mod applications;
mod change_password;
mod custom_domains;
mod endpoints;
mod errors;
mod gitlab;
mod k8s;
mod mod_user;
mod pw_reset;
mod userdbs;

use crate::{
    adapters::{
        ldap::LdapAdapter, ApplicationsAdapter, DiscordAdapter, DomainsAdapter, EmailsAdapter,
        EndpointsAdapter, GitlabAdapter, KubernetesAdapter, LocalKerberosAdapter, Pool,
        PwResetsAdapter, UserDBsAdapter,
    },
    config::Config,
};
use gitlab::GitlabCache;

pub struct App {
    applications: ApplicationsAdapter,
    krb: LocalKerberosAdapter,
    k8s: KubernetesAdapter,
    ldap: LdapAdapter,
    email: EmailsAdapter,
    discord: DiscordAdapter,
    endpoints: EndpointsAdapter,
    domains: DomainsAdapter,
    userdbs: UserDBsAdapter,
    gitlab: GitlabAdapter,
    gitlab_cache: GitlabCache,
    pw_resets: PwResetsAdapter,
}

impl App {
    pub async fn new(config: &Config, tera: Arc<Tera>) -> Result<App> {
        let db_pool = Pool::connect(&config.db.db_url).await?;
        sqlx::migrate!("../migrations/").run(&db_pool).await?;

        let krb = LocalKerberosAdapter::new(&config.kerberos);
        let email = EmailsAdapter::new(&config.smtp, tera);
        let k8s = KubernetesAdapter::new(&config.kubernetes)?;
        let gitlab = GitlabAdapter::new(&config.gitlab);
        let ldap = LdapAdapter::new(&config.ldap).await?;
        let applications = ApplicationsAdapter::new(db_pool.clone());
        let pw_resets = PwResetsAdapter::new(db_pool.clone());
        let endpoints = EndpointsAdapter::new(db_pool.clone());
        let domains = DomainsAdapter::new(db_pool.clone());
        let discord = DiscordAdapter::new(config.discord_webhook.clone());

        let userdb_pool = MySqlPool::connect(&config.db.userdb_url).await?;
        let userdbs = UserDBsAdapter::new(db_pool, userdb_pool);

        Ok(Self {
            userdbs,
            applications,
            gitlab,
            krb,
            k8s,
            ldap,
            email,
            discord,
            endpoints,
            pw_resets,
            domains,
            gitlab_cache: GitlabCache::default(),
        })
    }
}
