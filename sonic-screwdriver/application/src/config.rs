//! Config for different parts of the application

use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    pub http: Http,
    pub kerberos: Kerberos,
    pub auth: Auth,
    pub db: Databases,
    pub smtp: Smtp,
    pub ldap: Ldap,
    pub kubernetes: Kubernetes,
    pub gitlab: Gitlab,

    /// Discord webhook URL (for new applications)
    pub discord_webhook: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Http {
    /// Address to bind to
    pub host: String,

    /// Port to listen on
    pub port: u16,

    /// Directory for static resources
    pub static_dir: String,

    /// Directory for template resources
    pub template_dir: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Kerberos {
    pub principal: String,
    pub realm: String,
    pub password: String,
}

#[derive(Deserialize, Debug, Clone)]
/// Contains config for our oauth provider
pub struct Auth {
    pub auth_uri: String,
    pub jwk_uri: String,
    pub token_uri: String,
    pub end_session_uri: String,
    pub client_id: String,
    pub client_secret: String,
    pub login_uri: String,
    pub redirect_uri: String,
    pub jwt_issuer: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Databases {
    /// Main Database connection URL
    pub db_url: String,

    /// Database connection URL for the server used for userdbs
    pub userdb_url: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Ldap {
    pub host: String,
    pub port: u16,
    /// The base DN of the users subtree
    pub users_base: String,
    /// User to bind as
    pub bind_dn: String,
    /// Password to bind with
    pub bind_pw: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Smtp {
    pub host: String,
    pub port: u16,
    pub username: String,
    pub password: String,
    pub starttls: bool,
    pub ssl: bool,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Kubernetes {
    pub url: String,
    pub token: String,
    pub ca_cert: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Gitlab {
    pub url: String,
    pub api_key: String,
    pub internal_secret: String,
}
