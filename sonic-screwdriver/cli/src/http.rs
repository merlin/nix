use std::{error::Error, fmt::Display};

use anyhow::{anyhow, bail, Context, Result};
use base64::{prelude::BASE64_STANDARD, Engine};
use cross_krb5::{ClientCtx, InitiateFlags};
use serde::Serialize;
use sonic_types::{
    endpoint::NewEndpoint,
    userdb::UserDB,
    web::{
        endpoints::{CreateEndpointRequest, DeleteEndpointRequest, ListEndpointsResponse},
        userdbs::{ManageUserDBRequest, UserDBConnectionDetails},
    },
};
use ureq::{serde::Deserialize, OrAnyStatus, Request};

use log::*;

// const HOST: &str = "https://console.tardisproject.uk";
const HOST: &str = "http://localhost:8000";

pub struct Client {
    http: ureq::Agent,
}

impl Client {
    pub fn new_authenticated() -> Result<Self> {
        let http = ureq::builder().redirects(0).build();
        let this = Self { http };
        this.ensure_auth()
            .context("while authenticating to console")?;

        Ok(this)
    }

    fn url(&self, path: &str) -> String {
        format!("{}/{}", HOST, path)
    }

    fn ensure_auth(&self) -> Result<()> {
        debug!("Starting to authenticate");

        // perform oauth flow, authenticating with kerberos
        debug!("Making initial request to /login");
        let auth_req = self
            .http
            .get(&self.url("login"))
            .call()
            .context("while making login request to console")?;

        let redirect_url = auth_req
            .header("Location")
            .ok_or_else(|| anyhow!("expected login page to redirect"))?;

        if redirect_url.starts_with(HOST) {
            return Ok(());
        } else if !redirect_url.starts_with("https://id.tardisproject.uk") {
            bail!("login page did not redirect to console or oauth gateway")
        }

        // TODO: Deal with user not yet being kerberos authenticated
        debug!("Initialising kerberos context");
        let (_, token) = ClientCtx::new(
            InitiateFlags::empty(),
            Some("tcmal@TARDISPROJECT.UK"),
            "http/id.tardisproject.uk@TARDISPROJECT.UK",
            None,
        )
        .expect("new");

        debug!("Authenticating to keycloak");
        let auth_resp = self
            .http
            .get(redirect_url)
            .set(
                "Authorization",
                &format!("Negotiate {}", BASE64_STANDARD.encode(&*token)),
            )
            .call()?;

        let cb_url = auth_resp
            .header("location")
            .ok_or_else(|| anyhow!("keycloak did not redirect"))?;
        if !cb_url.starts_with(HOST) {
            bail!("keycloak did not redirect back to console");
        }

        debug!("Following callback");
        let cb_resp = self
            .http
            .get(cb_url)
            .call()
            .context("Error making callback URL")?;

        if cb_resp.header("location") != Some("/") {
            bail!("oauth callback did not succeed");
        }

        Ok(())
    }

    fn build_get(&self, path: &str) -> Request {
        debug!("GET /{}", path);
        self.http.get(&self.url(path))
    }

    fn build_post(&self, path: &str) -> Request {
        debug!("POST /{}", path);
        self.http.post(&self.url(path))
    }

    fn build_delete(&self, path: &str) -> Request {
        debug!("DELETE /{}", path);
        self.http.delete(&self.url(path))
    }

    fn get<R: for<'a> Deserialize<'a>>(&self, path: &str) -> Result<R> {
        let body = self.build_get(path).call().or_any_status()?.into_string()?;

        debug!("response body: {}", body);
        serde_json::from_str(&body)
            .map_err(Into::into)
            .and_then(lift_json_err)
    }

    fn post<T: Serialize, R: for<'a> Deserialize<'a>>(&self, path: &str, req: &T) -> Result<R> {
        let body = self
            .build_post(path)
            .send_json(req)
            .or_any_status()?
            .into_string()?;

        debug!("response body: {}", body);

        serde_json::from_str(&body)
            .map_err(Into::into)
            .and_then(lift_json_err)
    }

    pub fn list_userdbs(&self) -> Result<Vec<UserDB>> {
        self.get("api/userdbs")
    }

    pub fn create_userdb(&self, req: &ManageUserDBRequest) -> Result<UserDBConnectionDetails> {
        self.post("api/userdbs", req)
    }

    pub fn reset_userdb(&self, req: &ManageUserDBRequest) -> Result<UserDBConnectionDetails> {
        self.post("api/userdbs/reset", req)
    }

    pub fn delete_userdb(&self, req: &ManageUserDBRequest) -> Result<()> {
        let resp = self
            .build_delete("api/userdbs")
            .send_json(req)
            .or_any_status()?;

        if resp.status() / 100 != 2 {
            Err(resp.into_json::<JSONError>()?.into())
        } else {
            Ok(())
        }
    }

    pub fn list_endpoints(&self) -> Result<ListEndpointsResponse> {
        self.get("api/endpoints")
    }

    pub fn create_endpoint(&self, req: &CreateEndpointRequest) -> Result<NewEndpoint> {
        self.post("api/endpoints", req)
    }

    pub fn delete_endpoint(&self, req: &DeleteEndpointRequest) -> Result<()> {
        let resp = self
            .build_delete("api/endpoints")
            .send_json(req)
            .or_any_status()?;

        if resp.status() / 100 != 2 {
            Err(resp.into_json::<JSONError>()?.into())
        } else {
            Ok(())
        }
    }
}

fn lift_json_err<T>(resp: APIResponse<T>) -> Result<T> {
    match resp {
        APIResponse::Err { err } => Err(err.into()),
        APIResponse::Ok(v) => Ok(v),
    }
}

#[derive(Deserialize)]
#[serde(untagged)]
enum APIResponse<T> {
    Err {
        #[serde(flatten)]
        err: JSONError,
    },
    Ok(T),
}

#[derive(Deserialize, Debug)]
pub struct JSONError {
    #[serde(rename = "success")]
    _success: bool,
    message: String,
}

impl Error for JSONError {}
impl Display for JSONError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}
