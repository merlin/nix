#!/usr/bin/env sh
set -x
(cat | sudo tee /etc/krb5.conf) <<EOF
[libdefaults]
        default_realm = TARDISPROJECT.UK
        kdc_timesync = 1
        ccache_type = 4
        forwardable = true
        proxiable = true
        dns_lookup_kdc = false
        dns_lookup_realm = false
[appdefaults]
        dns_lookup_kdc = false
        dns_lookup_realm = false
[realms]
        TARDISPROJECT.UK = {
                admin_server = localhost
                kpasswd_server = localhost
                kdc = localhost
        }
EOF
sudo sysctl net.ipv4.ip_unprivileged_port_start=0
ssh -N enclave.tardisproject.uk -L 88:localhost:88
