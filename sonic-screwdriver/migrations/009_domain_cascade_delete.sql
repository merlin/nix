-- this migration sets it so that when a domain is deleted, the relevant endpoints are too
-- unfortunately, it needs to re-create the table for this, so we move things about to avoid losing data

PRAGMA foreign_keys=off;

ALTER TABLE endpoint RENAME TO _endpoint_old;

CREATE TABLE IF NOT EXISTS "endpoint" (
       "prefix" text(50) NOT NULL,
       "domain_id" INTEGER NOT NULL,
       "target_ip" text(15) NOT NULL,
       "target_port" INTEGER NOT NULL,
       "target_is_https" BOOLEAN NOT NULL,
       "strip_prefix" BOOLEAN DEFAULT FALSE,
       "force_https" BOOLEAN DEFAULT TRUE,
       "host_override" text(50) DEFAULT NULL,

       PRIMARY KEY (prefix, domain_id),
       FOREIGN KEY(domain_id) REFERENCES domain(id) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO endpoint SELECT * FROM _endpoint_old;

DROP TABLE _endpoint_old;

PRAGMA foreign_keys=on;
