CREATE TABLE IF NOT EXISTS "application" (
       "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
       "first_name" text(50) NOT NULL,
       "last_name" text(50) NOT NULL,
       "email" text(150) NOT NULL,
       "preferred_username" text(50) NOT NULL,
       "graduation_year" integer NULL,
       "is_legacy_user" boolean NOT NULL,
       "comments" text(500) NULL,
       "accepted" boolean NULL,
       "decision_made_on" text NULL
);
