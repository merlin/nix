CREATE TABLE IF NOT EXISTS "userdb" (
       "owner" text(50) NOT NULL,
       "name" text(30) NOT NULL,
       PRIMARY KEY (owner, name)
);
