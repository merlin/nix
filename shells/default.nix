pkgs:
pkgs.devshell.mkShell {
  commands = let
    pkgWithCategory = category: package: {inherit package category;};
    manage = pkgWithCategory "manage";
    lint = pkgWithCategory "lint";
    deploy = pkgWithCategory "deploy";
  in
    with pkgs; [
      (manage agenix)
      (manage age)
      (lint alejandra)
      (lint deadnix)
      (deploy colmena)
      (deploy ansible)

      {
        name = "eval_system";
        help = "Run the evaluation stage for a NixOS host - doing basic checks but not building anything";
        category = "manage";
        command = "$PRJ_ROOT/scripts/eval_system $@";
      }
      {
        name = "nvd_system";
        help = "Build a NixOS host remotely, and show the package changes compared to the production version.";
        category = "lint";
        command = "$PRJ_ROOT/scripts/nvd_system $@";
      }
    ];

  packages = [pkgs.sshpass];

  env = [
    {
      name = "LC_ALL";
      value = "C.UTF-8";
    }
  ];
}
