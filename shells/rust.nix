pkgs:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    (rust-bin.stable.latest.default.override {
      extensions = ["rust-src" "rust-analyzer"];
    })
    jq

    clang
    krb5

    pkg-config
    openssl.dev
    mariadb-client
  ];

  PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkg-config";
  LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";
  LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath [pkgs.openssl];
}
