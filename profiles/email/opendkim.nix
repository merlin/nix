{
  config,
  pkgs,
  ...
}: {
  services.opendkim = let
    cfg = config.services.opendkim;
  in {
    enable = true;

    selector = "dkim2";
    domains = "tardisproject.uk,schlatt.gay";
    configFile = pkgs.writeText "opendkim.conf" ''
      UMask 0002
      Socket ${cfg.socket}
    '';
  };

  systemd.services.postfix = {
    after = ["opendkim.service"];
    requires = ["opendkim.service"];
  };

  users.extraUsers.${config.services.postfix.user}.extraGroups = [config.services.opendkim.group];
}
