{
  pkgs,
  config,
  lib,
  ...
}: let
  ldapSenderLoginMap = "ldap:${config.age.secrets.postfixLdapConf.path}";
  submissionOptions = {
    smtpd_tls_security_level = "encrypt";
    smtpd_sasl_auth_enable = "yes";
    smtpd_client_restrictions = "permit_sasl_authenticated,reject";
    smtpd_sender_login_maps = ldapSenderLoginMap;
    smtpd_relay_restrictions = "permit_sasl_authenticated,reject";
    smtpd_sender_restrictions = "reject_sender_login_mismatch";
    smtpd_recipient_restrictions = "reject_non_fqdn_recipient,reject_unknown_recipient_domain,permit_sasl_authenticated,reject";
    cleanup_service_name = "submission-header-cleanup";
  };
in {
  age.secrets = {
    postfixLdapConf = {
      file = ../../secrets/postfixLdapConf.age;
      owner = config.services.postfix.user;
      group = config.services.postfix.group;
      mode = "0440";
    };
    postfixLdapAliasesConf = {
      file = ../../secrets/postfixLdapAliasesConf.age;
      owner = config.services.postfix.user;
      group = config.services.postfix.group;
      mode = "0440";
    };
  };

  services.postfix = let
    origin = lib.subdomain "mail";
  in {
    enable = true;

    inherit origin;
    hostname = origin;

    useSrs = true;

    sslKey = "/var/lib/caddy/.local/share/caddy/certificates/acme-v02.api.letsencrypt.org-directory/${lib.subdomain "mail"}/${lib.subdomain "mail"}.key";
    sslCert = "/var/lib/caddy/.local/share/caddy/certificates/acme-v02.api.letsencrypt.org-directory/${lib.subdomain "mail"}/${lib.subdomain "mail"}.crt";

    enableSubmission = true;
    enableSubmissions = true;

    config = {
      # Basic
      mydestination = "";
      myhostname = "${lib.subdomain "mail"}";
      recipient_delimiter = "+";
      disable_vrfy_command = true;

      # LDAP
      virtual_mailbox_domains = "tardisproject.uk,schlatt.gay";
      virtual_alias_maps = "ldap:${config.age.secrets.postfixLdapAliasesConf.path}";
      virtual_mailbox_maps = ldapSenderLoginMap;

      # Pass to dovecot
      virtual_uid_maps = "static:5000"; # ignored
      virtual_gid_maps = "static:5000"; # ignored
      virtual_transport = "lmtp:unix:/run/dovecot2/dovecot-lmtp";

      # Auth with dovecot
      smtpd_sasl_type = "dovecot";
      smtpd_sasl_path = "/run/dovecot2/auth";
      smtpd_sasl_auth_enable = true;
      smtpd_sasl_security_options = "noanonymous";
      # smtpd_sasl_local_domain = "tardisproject.uk";

      # Submission only through submission port
      smtpd_relay_restrictions = [
        "reject_unauth_destination"
      ];
      smtpd_recipient_restrictions = [
        "reject_unauth_destination"
        "reject_invalid_hostname"
        "reject_unauth_pipelining"
        "reject_non_fqdn_sender"
        "reject_unknown_sender_domain"
        "reject_non_fqdn_recipient"
        "reject_unknown_recipient_domain"
        "reject_rbl_client sbl-xbl.spamhaus.org"
        "permit"
      ];

      # TLS / Security
      lmtp_destination_recipient_limit = "1"; # Avoid leakage of X-Original-To, X-Delivered-To headers between recipients
      smtpd_tls_eecdh_grade = "strong";
      smtpd_tls_security_level = "may";
      smtp_tls_ciphers = "high";
      smtpd_tls_ciphers = "high";
      smtp_tls_mandatory_ciphers = "high";
      smtpd_tls_mandatory_ciphers = "high";
      smtpd_tls_protocols = "TLSv1.3, TLSv1.2, TLSv1.1, !TLSv1, !SSLv2, !SSLv3";
      smtp_tls_protocols = "TLSv1.3, TLSv1.2, TLSv1.1, !TLSv1, !SSLv2, !SSLv3";
      smtpd_tls_mandatory_protocols = "TLSv1.3, TLSv1.2, TLSv1.1, !TLSv1, !SSLv2, !SSLv3";
      smtp_tls_mandatory_protocols = "TLSv1.3, TLSv1.2, TLSv1.1, !TLSv1, !SSLv2, !SSLv3";
      smtpd_tls_mandatory_exclude_ciphers = "MD5, DES, ADH, RC4, PSD, SRP, 3DES, eNULL, aNULL";
      smtpd_tls_exclude_ciphers = "MD5, DES, ADH, RC4, PSD, SRP, 3DES, eNULL, aNULL";
      smtp_tls_mandatory_exclude_ciphers = "MD5, DES, ADH, RC4, PSD, SRP, 3DES, eNULL, aNULL";
      smtp_tls_exclude_ciphers = "MD5, DES, ADH, RC4, PSD, SRP, 3DES, eNULL, aNULL";
      tls_preempt_cipherlist = true;
      smtpd_tls_auth_only = true;
      smtpd_tls_loglevel = "1";

      # Configure a non blocking source of randomness
      tls_random_source = "dev:/dev/urandom";

      # Spam checking
      smtpd_milters = ["unix:/run/opendkim/opendkim.sock" "unix:/run/rspamd/rspamd-milter.sock"];
      non_smtpd_milters = ["unix:/run/opendkim/opendkim.sock" "unix:/run/rspamd/rspamd-milter.sock"];
      milter_default_action = "tempfail";
      milter_protocol = "6";
      milter_mail_macros = "i {mail_addr} {client_addr} {client_name} {auth_type} {auth_authen} {auth_author} {mail_addr} {mail_host} {mail_mailer}";
    };

    submissionOptions = submissionOptions;
    submissionsOptions = submissionOptions;

    masterConfig = {
      bounce = {command = "discard";};
      "submission-header-cleanup" = {
        type = "unix";
        private = false;
        chroot = false;
        maxproc = 0;
        command = "cleanup";
        args = [
          "-o"
          "header_checks=pcre:${
            pkgs.writeText "submission_header_cleanup_rules" ''
              # Removes sensitive headers from mails handed in via the submission port.
              # See https://thomas-leister.de/mailserver-debian-stretch/
              # Uses "pcre" style regex.

              /^Received:/            IGNORE
              /^X-Originating-IP:/    IGNORE
              /^X-Mailer:/            IGNORE
              /^User-Agent:/          IGNORE
              /^X-Enigmail:/          IGNORE

              # Replaces the user submitted hostname with the server's FQDN to hide the
              # user's host or network.

              /^Message-ID:\s+<(.*?)@.*?>/ REPLACE Message-ID: <$1@mail.tardisproject.uk>
            ''
          }"
        ];
      };
    };
  };
  services.pfix-srsd = {
    domain = "tardisproject.uk";
  };

  services.fail2ban.jails.postfix = ''
    enabled = true
    filter = postfix
    maxretry = 5
  '';
}
