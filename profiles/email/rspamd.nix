{config, ...}: {
  services.rspamd = {
    enable = true;

    locals = {
      "milter_headers.conf" = {
        text = ''
          extended_spam_headers = yes;
        '';
      };
      "redis.conf" = {
        text = ''
          servers = "127.0.0.1:${builtins.toString config.services.redis.servers.rspamd.port}";
        '';
      };
      "classifier-bayes.conf" = {
        text = ''
          cache {
            backend = "redis";
          }
        '';
      };
    };

    overrides = {
      "milter_headers.conf" = {
        text = ''
          extended_spam_headers = true;
        '';
      };
    };

    workers = {
      rspamd_proxy = {
        type = "rspamd_proxy";
        bindSockets = [
          {
            socket = "/run/rspamd/rspamd-milter.sock";
            mode = "0664";
          }
        ];
        count = 1; # Do not spawn too many processes of this type
        extraConfig = ''
          milter = yes; # Enable milter mode
          timeout = 120s; # Needed for Milter usually

          upstream "local" {
            default = yes; # Self-scan upstreams are always default
            self_scan = yes; # Enable self-scan
          }
        '';
      };
      controller = {
        type = "controller";
        count = 1;
        bindSockets = ["*:11333"];
        includes = [];
        extraConfig = ''
          static_dir = "''${WWWDIR}"; # Serve the web UI static assets
        '';
      };
    };
  };

  services.redis.servers.rspamd = {
    enable = true;
    port = 6378;
  };

  systemd.services.rspamd = {
    requires = ["redis-rspamd.service"];
    after = ["redis-rspamd.service"];
  };

  systemd.services.postfix = {
    after = ["rspamd.service"];
    requires = ["rspamd.service"];
  };

  users.extraUsers.${config.services.postfix.user}.extraGroups = [config.services.rspamd.group];
}
