{...}: {
  imports = [
    ./rspamd.nix
    ./dovecot.nix
    ./postfix.nix
    ./opendkim.nix
  ];

  networking.firewall.allowedTCPPorts = [
    25 # incoming SMTP (starttls)
    143 # imap (starttls)
    465 # submission (ssl)
    587 # submission (starttls)
    993 # imap (ssl)
  ];

  services.fail2ban = {
    enable = true;
    ignoreIP = [
      "192.168.0.0/24"
    ];
  };
}
