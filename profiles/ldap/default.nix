{pkgs, ...}: {
  networking.firewall.allowedTCPPorts = [389];

  services.saslauthd = {
    enable = true;
    mechanism = "kerberos5";
  };

  services.openldap = {
    enable = true;
    # TODO: This should be upstreamed in a less scuffed way.
    package = pkgs.openldap.overrideAttrs (_: prev: {
      configureFlags = prev.configureFlags ++ ["--enable-spasswd" "--with-cyrus-sasl"];
      buildInputs = with pkgs; [
        (cyrus_sasl.overrideAttrs (_: _: {
          postInstall = ''
            cat >$out/lib/sasl2/slapd.conf <<EOF
            pwcheck_method: saslauthd
            saslauthd_path: /var/run/saslauthd/mux
            EOF
          '';
        }))
        db
        libsodium
        libtool
        openssl
        systemdMinimal
        libxcrypt
        systemdMinimal
      ];
      doCheck = false;
    });

    settings = let
      ldap = pkgs.openldap;
    in {
      attrs = {
        olcLogLevel = ["stats"];
      };
      children = {
        "cn=schema".includes = [
          "${ldap}/etc/schema/core.ldif"
          "${ldap}/etc/schema/cosine.ldif"
          "${ldap}/etc/schema/nis.ldif"
          "${ldap}/etc/schema/inetorgperson.ldif"
          ./krb.ldif
          ./vmail.ldif
          ./tardis.ldif
        ];
        "olcDatabase={-1}frontend" = {
          attrs = {
            objectClass = "olcDatabaseConfig";
            olcDatabase = "{-1}frontend";
            olcAccess = [
              ''{0}to dn.base="" by * read''
              ''{1}to dn.base="cn=subschema" by * read''
              ''{2}to * by * none stop''
            ];
          };
        };
        "olcDatabase={0}config" = {
          attrs = {
            objectClass = "olcDatabaseConfig";
            olcDatabase = "{0}config";
            olcAccess = ["{0}to * by * none break"];
          };
        };
        "olcDatabase={1}mdb" = {
          attrs = {
            objectClass = ["olcDatabaseConfig" "olcMdbConfig"];
            olcDatabase = "{1}mdb";
            olcDbDirectory = "/var/lib/openldap/tardis";
            # If recreating locally, you can set this to bootstrap the uid=admin account, which is what we actually use
            # olcRootDN = "cn=admin,dc=tardisproject,dc=uk";
            # olcRootPW = "1234";
            olcDbIndex = [
              "objectClass eq"
              "cn pres,eq"
              "uid pres,eq"
              "sn pres,eq,subany"
            ];
            olcSuffix = "dc=tardisproject,dc=uk";
            olcAccess = [
              ''{0}to * by dn.exact="uid=admin,dc=tardisproject,dc=uk" write by users read by anonymous auth by self write by * none''
              ''{1}to attrs=krbPrincipalKey by anonymous auth by dn.exact="uid=kdc,ou=services,dc=tardisproject,dc=uk" read by dn.exact="uid=kadmin,ou=services,dc=tardisproject,dc=uk" write by self write by * none''
              ''{2}to dn.subtree="cn=krb,dc=tardisproject,dc=uk" by dn.exact="uid=kdc,ou=services,dc=tardisproject,dc=uk" read by dn.exact="uid=kadmin,ou=services,dc=tardisproject,dc=uk" write by self write by * none''
            ];
          };
        };
      };
    };
  };
}
