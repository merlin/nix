{config, ...}: {
  networking.firewall.allowedTCPPorts = [8000];

  age.secrets.vaultwardenEnv.file = ../secrets/vaultwardenEnv.age;
  services.vaultwarden = {
    enable = true;
    environmentFile = config.age.secrets.vaultwardenEnv.path;
    config = {
      dataFoilder = "bitwarden_rs";
      useSyslog = true;
      logLevel = "Info";
      iconService = "internal";

      signupsAllowed = false;
      signupsVerify = true;
      signupsDomainsWhitelist = "tardisproject.uk";
      invitationsAllowed = true;

      domain = "https://pw.tardisproject.uk";
      rocket_address = "0.0.0.0";
      rocket_port = 8000;

      smtpHost = "mail.tardisproject.uk";
      smtpFrom = "vaultwarden@tardisproject.uk";
      smtpFromName = "Vaultwarden";
      smtpSecurity = "starttls";
      smtpPort = 587;
      smtpTimeout = 15;
    };
  };
}
