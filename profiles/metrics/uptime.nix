{pkgs, ...}: {
  services.netdata.configDir = {
    "go.d/httpcheck.conf" = let
      mkJob = name: url: ''
        - name: "${name}"
          url: "${url}"
      '';
    in
      pkgs.writeText "netbox-httpcheck" ''
        jobs:
        ${mkJob "Homepage" "https://tardisproject.uk"}
        ${mkJob "User homepages" "https://tardis.ac"}

        ${mkJob "Console" "https://console.tardisproject.uk"}
        ${mkJob "Gitlab" "https://git.tardisproject.uk"}
        ${mkJob "Keycloak" "https://id.tardisproject.uk"}
        ${mkJob "User MinIO" "https://minio.on.tardis.ac/minio/health/live"}
      '';

    "go.d/portcheck.conf" = let
      mkJob = name: host: port: ''
        - name: "${name}"
          host: "${host}"
          ports:
          - ${builtins.toString port}
      '';
    in
      pkgs.writeText "netbox-httpcheck" ''
        jobs:
        ${mkJob "User SSH" "tardisproject.uk" 22}

        ${mkJob "SMTP (Submission)" "mail.tardisproject.uk" 587}
        ${mkJob "IMAP" "mail.tardisproject.uk" 143}

        ${mkJob "User MySQL" "userdbs.tardisproject.uk" 3306}
      '';
  };
}
