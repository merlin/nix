{...}: {
  services.netdata.prometheusScrapers = let
    entryForHost = host: ''
      - name: ${host}-ssacli
        url: http://${host}.internal.tardisproject.uk:9101/metrics
        selector:
          allow:
            - logical_device_status
    '';
  in [
    ''
      ${entryForHost "canton"}
      ${entryForHost "sontaran"}
    ''
  ];
}
