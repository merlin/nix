{
  pkgs,
  lib,
  config,
  ...
}: {
  system.stateVersion = "23.05";

  environment.systemPackages = with pkgs; [
    vim
    curl
    dnsutils
    git
    nvd
  ];

  networking.domain = lib.consts.clusterDomain;

  # Fixes issues with kerberos
  networking.hosts = let
    hosts = ["${config.networking.fqdn}" "${config.networking.hostName}"];
  in {
    "127.0.0.1" = hosts ++ ["localhost"];
    "127.0.0.2" = hosts;
    "::1" = hosts;
  };
  networking.hostFiles = let
    stringHosts = let
      oneToString = set: ip: ip + " " + lib.concatStringsSep " " set.${ip} + "\n";
      allToString = set: lib.concatMapStrings (oneToString set) (lib.attrNames set);
    in
      pkgs.writeText "string-hosts" (allToString (lib.filterAttrs (_: v: v != []) config.networking.hosts));
    extraHosts = pkgs.writeText "extra-hosts" config.networking.extraHosts;
  in
    lib.mkBefore [stringHosts extraHosts];

  networking.firewall = {
    enable = true;
    extraInputRules = ''
      ip4 saddr { 47.76.35.19, 64.124.8.63 } tcp dport 80 deny
      ip4 saddr { 47.76.35.19, 64.124.8.63 } tcp dport 443 deny
    '';
  };

  nix = {
    nixPath = [
      "nixpkgs=${pkgs.inputs.nixos}"
    ];
    gc = {
      automatic = true;
      options = "--delete-older-than 7d";
    };
    # If this isn't set, then every system changes whenever a commit is made
    # Which is super annoying
    registry = lib.mkForce {};

    settings = {
      # Improve nix store disk usage
      auto-optimise-store = true;

      # Prevents impurities in builds
      sandbox = true;
    };
  };

  # PKI
  security.smallstep.defaults = {
    caURL = "https://ca.tardisproject.uk";
    caRootPath = pkgs.writeText "root-ca.crt" lib.consts.caCert;
  };
  security.pki.certificates = [lib.consts.caCert];

  # SSH Access
  services.openssh = {
    enable = true;
    openFirewall = true;
    settings.PermitRootLogin = "prohibit-password";
  };
  users.users.root.openssh.authorizedKeys.keys = [lib.consts.rootPubKey];

  # Swap file
  swapDevices = [
    {
      device = "/var/swapfile";
      size = 4 * 1024;
    }
  ];
}
