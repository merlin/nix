{
  config,
  pkgs,
  ...
}: {
  environment.systemPackages = [pkgs.sonic-screwdriver];

  networking.firewall.allowedTCPPorts = [6666];
  imports = [
    ./krb/client.nix
  ];

  age.secrets.sonicRocketToml.file = ../secrets/sonicRocketToml.age;
  systemd.services.sonic-http = {
    description = "Sonic Screwdriver HTTP (tardis-console)";
    path = [pkgs.sonic-screwdriver pkgs.krb5];
    script = ''
      export SONIC_HTTP_CONFIG="$CREDENTIALS_DIRECTORY/Sonic.toml";
      ${pkgs.sonic-screwdriver}/bin/sonic-http;
    '';
    serviceConfig = {
      LoadCredential = "Sonic.toml:${config.age.secrets.sonicRocketToml.path}";
      DynamicUser = true;
      StateDirectory = "sonic";
    };
    wantedBy = ["multi-user.target"];
  };
}
