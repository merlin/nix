{
  pkgs,
  lib,
  ...
}: {
  users.users.nginx.extraGroups = ["users"];
  users.groups.nginx.gid = lib.mkForce 33;
  users.groups.users.gid = lib.mkForce 1001;

  environment.systemPackages = [pkgs.nfs-utils];
  systemd.mounts = [
    {
      where = "/var/home/";
      type = "nfs4";
      what = "192.168.0.130:/home";
      options = "rw,async";
    }
  ];

  networking.firewall.allowedTCPPorts = [8181];
  services.nginx = {
    enable = true;
    appendHttpConfig = "disable_symlinks off;";
    virtualHosts.userhomes = {
      serverName = "~^(?<username>.+)\.tardis\.ac$";
      listen = [
        {
          addr = "0.0.0.0";
          port = 8181;
          ssl = false;
        }
      ];
      root = "/var/home/$username/www/";
    };
  };
  systemd.services.nginx.after = ["var-home.mount"];
}
