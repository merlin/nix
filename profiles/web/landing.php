<html>

<head>
  <style>
   body {
       max-width: 60ch;
       margin: 0.8rem auto;
       font-family: sans-serif;
       text-align: center;
   }

   img {
       margin: 0 auto;
   }

   p {
       padding-bottom: 1rem;
   }

   a.btn {
       border-radius: 1rem 1rem;
       text-decoration: none;
       background-color: #405cf5;
       border-radius: 6px;
       border-width: 0;
       box-shadow: rgba(50, 50, 93, .1) 0 0 0 1px inset,rgba(50, 50, 93, .1) 0 2px 5px 0,rgba(0, 0, 0, .07) 0 1px 1px 0;
       color: #fff;
       padding: 1rem 0.9rem;
       text-align: center;
   }

  </style>
</head>
<body>
  <img src="https://tardisproject.uk/_astro/tardis.CPngXvGG_1DCswB.webp" />
  <h1>We&apos;ve moved.</h1>
  <div>
    <p>This domain used to belong to the TARDIS project, but as we&apos;re no longer a part of the University, we&apos;re moving away from it.</p>
    <p>The content you're looking for might still be available at our new domain, <a href="https://tardisproject.uk">tardisproject.uk</a>, but please be aware the University of Edinburgh has not reviewed and does not necessarily approve of it.</p>
    <!--  maya 28/03/25: dns was broken, so i assume that's why this is gone, bring it back now, though. -->
    
  </div>
  <a class="btn" href="https://tardisproject.uk">Our website</a>
</body>
