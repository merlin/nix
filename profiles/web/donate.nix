{
  pkgs,
  lib,
  config,
  ...
}: {
  services.caddy.virtualHosts."http://donate.tardisproject.uk" = {
    hostName = "donate.tardisproject.uk";
    extraConfig = lib.mkBefore ''
      @civiprivate {
          path /wp-content/uploads/civicrm/ConfigAndLog/*
          path /wp-content/uploads/civicrm/templates_c/*
          path /wp-content/uploads/civicrm/upload/*
          path /wp-content/uploads/civicrm/custom/*
      }
      error @civiprivate 403
    '';
  };

  services.wordpress = {
    webserver = "caddy";
    sites."donate.tardisproject.uk" = {
      plugins = {
        civicrm = pkgs.stdenv.mkDerivation rec {
          name = "civicrm";
          version = "5.79.1";
          src = pkgs.fetchurl {
            url = "https://storage.googleapis.com/civicrm/civicrm-stable/${version}/civicrm-${version}-wordpress.zip";
            sha256 = "3a0b294baac65414491b4664afc756f2c0d0b1b8563807f3bdc35948f0a3abcb";
          };
          nativeBuildInputs = [pkgs.unzip];
          installPhase = "mkdir -p $out; cp -R * $out/";
        };
      };
      extraConfig = ''
        define('DB_PASSWORD', "");
      '';
    };
  };

  age.secrets.civicrmCronSecrets = {
    file = ../../secrets/civicrmCronSecrets.age;
    owner = config.services.caddy.user;
    group = config.services.caddy.group;
    mode = "0440";
  };

  systemd.services.civicrm-scheduled = {
    serviceConfig = {
      Type = "oneshot";
      LoadCredential = "civicrm:${config.age.secrets.civicrmCronSecrets.path}";
      User = config.services.caddy.user;
      Group = config.services.caddy.group;
    };
    path = [pkgs.curl];

    script = ''
      source $CREDENTIALS_DIRECTORY/civicrm
      POST_AUTH="-d name=''${CIVI_USER} -d pass=''${CIVI_PASS} -d key=''${SITE_KEY}"
      curl -s ''${POST_AUTH} https://donate.tardisproject.uk/wp-content/plugins/civicrm/civicrm/bin/cron.php
    '';
  };

  systemd.timers.civicrm-scheduled = {
    wantedBy = ["timers.target"];
    partOf = ["civicrm-scheduled.service"];
    timerConfig.OnCalendar = ["*-*-* *:00:00"];
  };
}
