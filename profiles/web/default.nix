{config, ...}: {
  imports = [
    ./donate.nix
    ./dokuwiki.nix
    ./oauth2proxy.nix
    ./userhomes.nix
    ./landing.nix
  ];

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "sysmans+web@tardisproject.uk";

  services.caddy = {
    enable = true;
    email = "sysmans+caddy@tardisproject.uk";

    # Most of the config is in ./Caddyfile
    # Some other bits are configured by their relevant nix modules - see above.
    extraConfig = builtins.readFile ./Caddyfile;
  };
  networking.firewall.allowedTCPPorts = [2019];

  # Pass PDNS api key
  age.secrets.caddyEnv = {
    file = ../../secrets/caddyEnv.age;
    owner = config.services.caddy.user;
    group = config.services.caddy.group;
    mode = "0440";
  };
  systemd.services.caddy.serviceConfig.EnvironmentFile = config.age.secrets."caddyEnv".path;
}
